# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

## ALIAS

#test

#alias exit
alias e='exit'


#alias para que vscode abra chrome con links en vez de firefxo
#de una reputisima vez
alias google-chrome='google-chrome-stable'

#alias para copiar lo que se hizo en la facu
alias facu-cp="~/Documents/_soft/scripts/copiar-facu-docs"

#alias para mandar las screenshots sacadas con flameshot a la papelera
alias clean-shots="/home/yerba/Documents/_soft/scripts/clean-screenshots"

#alias para limpiar las configs que quedan cuando desinstalas programas
alias clean-config="/home/yerba/Documents/_soft/scripts/clean-configs"

#alias para levantar tu workflow de freecodecamp
alias workflow="/home/yerba/Documents/_soft/scripts/workflow"

#alias para que yt reproduzca una playlist o un video en yt
alias mpv-yt="~/Documents/_soft/scripts/mpv-yt-playlist"


#aias para bajar mp3 con youtube-dl, usando parametros
alias yt-mp3="/home/yerba/Documents/_soft/scripts/yt-mp3"

#reproducir mpv desde la terminal
alias mpv_terminal="mpv --no-config --vo=tct --volume=80"

#obtener el path de un archivo
alias path="readlink -f"

#alias para usar gnome-pomodoro
#este es para pausar o resumir dependiendo donde estes
alias pomo--pause_resume="gnome-pomodoro --pause-resume"
#estos resetan el timer
alias pomo-start="/home/yerba/Documents/_soft/scripts/gnome-pomodoro-scripts/pomo-start"
alias pomo-end="/home/yerba/Documents/_soft/scripts/gnome-pomodoro-scripts/pomo-end
"
#estos son los settings
alias pomo-settings="gnome-pomodoro --preferences"

#alias fdcd
alias fdcd="/home/yerba/Documents/_soft/scripts/fdcd"

#usar trash-cli en vez de rm. Si ya se que no es buena idea y no lo recomienda, q coma 1 huevo
alias rm="trash-put"

#usar fd para fdfind (basicamente para poder remplazar comando find)
alias fd="fdfind"

#alias fast upwards navigation cd   
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'

#alias update y upgrade
alias update='sudo apt update'
alias upgrade='sudo apt upgrade'
alias dist-upgrade='sudo apt dist-upgrade'

#alias open
alias open="xdg-open"

#alias list files
alias l='ls -aC'
#alias list files+hidden files
alias lh='ls -a'


#alias bashrc_edit
alias bashrc_edit="micro ~/.bashrc"
#alias inputrc_edit
alias inputrc_edit="micro ~/.inputrc"
#alias chrome_index
alias chrome_index="micro /home/yerba/GoogleDrive/chabon/_backups/startpage-tilde/index.html"

#alias xclip, lo uso para hacer pipe de outputs al copyboard
alias "_xclip"="xclip -selection c"

#Kate la puta madre no abras cosas todo en una sola ventana
alias kate="kate --new"

## OTRAS COSAS

#fzf opciones colores
#export FZF_DEFAULT_OPTS='
#  --color fg:242,bg:236,hl:65,fg+:15,bg+:239,hl+:108
#  --color info:108,prompt:109,spinner:108,pointer:168,marker:168
#'

#deshabilitar el fuckin control flow del 1980
stty -ixon

# To have colors for ls and all grep commands such as grep, egrep and zgrep
export CLICOLOR=1
export LS_COLORS='no=00:fi=00:di=00;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:*.xml=00;31:'
export GREP_OPTIONS='--color=auto'

# Color for manpages in less makes manpages a little easier to read
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#


########################### NO TOUCHY STUFF ########################

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
#############################FIN OG BASHRC ZONE##########################

## COSAS QUE GRAL AGREGO CON COMANDOS tipo "comando > bashrc", POR LO QUE ESTAN TODAS PEGADAS (no lo voy a arreglar). Lo comento cuando puedo:

#arreglar GREP_OPTIONS is deprecated
alias grep="/usr/bin/grep $GREP_OPTIONS"
unset GREP_OPTIONS
export PATH="$HOME/.rbenv/bin:$PATH"
#ruby shit
eval "$(rbenv init -)"
#USAR ARCHIVO DE .LS_COLORS para MAS PLACER COLORIDO
eval $(dircolors -b $HOME/.LS_COLORS)

#Agregar path de z.sh para poder usar z-jump (FROM: https://github.com/rupa/z)
. /home/yerba/Documents/_soft/scripts/z-jump/z.sh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
